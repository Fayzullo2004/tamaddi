import Users from "./Component/Users/Users";
import {Route, Routes} from "react-router-dom";
import Restaran from "./Component/Restaran/Restaran";
import Menu from "./Asis/Menu/Menu";
import Ramiz from "./Component/Restaran/Ramiz";
import FeedUp from "./Component/Restaran/FeedUp";

function App() {
  return (

      <div className="bg-gray-100 w-full lg:w-2/4 m-auto max-h-max">
        <Routes>
          <Route path='/' element={<Users/>}/>
          <Route path='restaran' element={<Restaran/>}/>
          <Route path='/ramiz' element={<Ramiz/>}/>
          <Route path='/feedup' element={<FeedUp/>}/>
          <Route path='menu' element={<Menu/>}/>
        </Routes>
      </div>
  );
}

export default App;
