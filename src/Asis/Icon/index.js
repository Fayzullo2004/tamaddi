export const HamburgerIcon = props => (
  <svg width="30" height="29" viewBox="0 0 30 29" fill="none" xmlns="http://www.w3.org/2000/svg">
    <line x1="28.1" y1="6.9" x2="1.9" y2="6.9" stroke="#393939" strokeWidth="3.8" strokeLinecap="round"/>
    <line x1="28.1" y1="14.9" x2="1.9" y2="14.9" stroke="#393939" strokeWidth="3.8" strokeLinecap="round"/>
    <line x1="28.1" y1="22.9" x2="1.9" y2="22.9" stroke="#393939" strokeWidth="3.8" strokeLinecap="round"/>
  </svg>
)