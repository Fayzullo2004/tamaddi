import React, {useState} from 'react';
import {HamburgerIcon} from "../../Asis/Icon";
import {Link} from "react-router-dom";

function Users() {
  const [user, setUser] = useState([
    {
      user: "Fayzullo",
      task: "Junior React programmer",
      id: 1,
      compile: false
    },
    {
      user: "Islom",
      task: "Junior SMM and React",
      id: 2,
      compile: false
    },
    {
      user: "Oybek",
      task: "Junior React programmer",
      id: 3,
      compile: false
    },
    {
      user: "Nozim",
      task: "Junior Python programmer",
      id: 4,
      compile: false
    }
  ])
  const [addUser , setAddUser ] = useState(false)
  const [value , setValue] = useState('')
  const [task , setTask] = useState('')

  const handleToggle = (id) => {
    setUser([...user.map((users) =>
      users.id === id ? {...users, compile : !users.compile} : {...users}
    )
    ])
  }

  const a = (user.filter(person => person.compile !== false).length)

  const Add = () => {
    if (value) {
     const newUser = {
       id : Math.random(),
       task : task,
       user : value,
       compile : false
     }
     setUser([...user , newUser])
    }
    setAddUser(prev => !prev)
    setValue("")
    setTask("")
  }


  return (
    <div className='bg-gray-100 h-screen'>

      <div className='flex px-2 md:px-12 pt-10 pb-20'>
        <HamburgerIcon/>

        <h1 className='mx-auto text-3xl font-semibold leading-7' >
          Total number of users {a}
        </h1>

        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold px-4 py-2 text-center rounded-full text-2xl"
        onClick={() => setAddUser(true)}
        >
          +
        </button>

      </div>

      {
        addUser === true &&
      <div className='w-full bg-gray-500 absolute h-screen'>

        <div className="w-full md:w-1/2 px-3 mt-12">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                 htmlFor="grid-last-name">
            First Name
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="grid-last-name" type="text" placeholder="Doe" value={value} onChange={(e) => setValue(e.target.value)}/>
        </div>

        <div className="w-full md:w-1/2 px-3 mt-12 mb-12">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                 htmlFor="grid-last-name">
            Last Name
          </label>
          <input
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            id="grid-last-name" type="text" placeholder="Doe" value={task} onChange={(e) => setTask(e.target.value)}/>
        </div>

        <div className='flex justify-center'>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-12"
          onClick={Add}
          >
            Button
          </button>
        </div>


        </div>
      }
      {user.map((users) => {
        return (
          <div className='px-2 md:px-12' key={users.id} >
            <div
              className={users.compile ? "w-full bg-green-800 text-white py-5 mb-2 rounded-xl flex cursor-pointer" : "w-full bg-white py-5 mb-2 rounded-xl flex cursor-pointer"}
              onClick={() => handleToggle(users.id) }
            >
              <div className='mr-12 ml-4 md:ml-12'>
                <div className='rounded-full bg-blue-900'>
                  <h1 className='text-white text-lg px-4 py-2'>{users.user.at(0)}</h1>
                </div>
              </div>

              <div>
                <h1 className='text-xl font-lg'>{users.user}</h1>
                <p className='text-base'>{users.task}</p>
              </div>

            </div>
          </div>
        )
      })}

      <ul  className='flex flex-wrap gap-4 mt-4 mx-2 md:mx-12 '>
      {
        user.filter(person => person.compile !== false).map(filterPerson => (
            <li key={filterPerson.id} className='list-none px-6 py-2 bg-gray-500 rounded-full text-white mr-0 text-center'>{filterPerson.user}</li>
        ))
      }
      </ul>

      <div className='flex justify-center'>

        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-2 fixed bottom-0">

          <Link to='restaran'>
            Button
          </Link>
        </button>

      </div>

    </div>
  );
}

export default Users;