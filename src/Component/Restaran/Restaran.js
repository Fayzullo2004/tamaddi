import React, {useEffect, useState} from 'react';
import feedup from  '../../Asis/img/FeedUp.jpg'
import ramiz from  '../../Asis/img/Ramiz.jpg'
import payOsh from  '../../Asis/img/payOsh.jpg'
import DoDopizza from  '../../Asis/img/Dodo.jpg'
import les from  '../../Asis/img/Les.jpg'
import black from  '../../Asis/img/Black.jpg'
import {Link, useNavigate} from "react-router-dom";
import {equals} from "ramda";


function Restaran() {

  const [state , setState ] = useState(0)
  const [restaurant, setRestaurant] = useState([
    {
      img : feedup,
      firstName : "FeedUp",
      secondName : "Fast Food • Пиццерия • Барбекю • Популярные",
      compile : false,
      id : 1
    },
    {
      img : ramiz,
      firstName : "Ramiz",
      secondName : "Национальная • Кондитерская",
      compile : false,
      id : 2
    },
    {
      img : payOsh,
      firstName : "Pay-Osh",
      secondName : "Национальная",
      compile : false,
      id : 3
    } ,
    {
      img : DoDopizza,
      firstName : "DoDo-Pizza",
      secondName : "Pizza",
      compile : false,
      id : 4
    },
    {
      img : les,
      firstName : "Les Ailes",
      secondName : "Fast Food · Популярные · Новинки",
      compile : false,
      id : 5
    },
    {
      img : black,
      firstName : "Black Star Burger",
      secondName : "Fast Food · Барбекю",
      compile : false,
      id : 6
    }
  ])

  function logo() {
    if (equals(restaurant.map((r) => r.id) === 1)) {
      return '/feedup'
    }

    if (equals(restaurant.map((r) => r.id) === 2)) {
      return '/ramiz'
    }

    if (equals(restaurant.map((r) => r.id) === 3)) {
      return '/feedUp'
    }

    if (equals(restaurant.id, 4)) {
      return
    }

    if (equals(restaurant.id, 5)) {
      return
    }
    return '/'
  }

  const handleToggle = (id) => {
    setState(id)
    console.log(state)
  }

 useEffect((id) => {
   setState(id)
   console.log(state)
 }, [state])


  return (
    <div className='px-4'>
      <h1 className='pb-12 text-4xl text-center font-semibold pt-5'>
        List of the Restaurant
      </h1>

      <div className='md:flex flex-wrap'>
        {restaurant.map(Restaurant => {
            return(
                <div
                  className={setState === Restaurant.id ?
                    "rounded-t-lg shadow-2xl shadow-gray-600 mb-12 w-full md:w-1/3 border-8 border-green-300"
                    :
                    "rounded-t-lg shadow-2xl shadow-gray-600 mb-12 w-full md:w-1/3"}
                     key={Restaurant.id}
                     onClick={() => handleToggle(Restaurant.id) }
                >
                  <img src={Restaurant.img} alt=""/>
                  <h1 className='text-2xl font-semibold pt-4 pl-5'>{Restaurant.firstName}</h1>
                  <p className='text-xs text-gray-500 pt-2 pb-4 pl-5'>{Restaurant.secondName}</p>
                </div>
            )
          })
        }
      </div>
      <div className='flex justify-center'>

        <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-12" onClick={logo}>
          <Link to={logo()}>
            Button
          </Link>
        </button>

      </div>
    </div>
  );
}

export default Restaran;
